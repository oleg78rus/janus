﻿using System;
using System.Collections.Generic;

namespace Rsdn.Janus.TaskBar
{
	public interface ITaskbarService
	{
		void Start(IntPtr handle);
		void Stop();
		void AddButtons(Dictionary<string, string[]> dictionary);
	}
}