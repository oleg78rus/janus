﻿namespace Rsdn.Janus.TaskBar
{
	public enum OverlayIconType
	{
		/// <summary>
		/// Нет иконки
		/// </summary>
		None,

		/// <summary>
		/// Иконка "Есть новые сообщения"
		/// </summary>
		New,

		/// <summary>
		/// Иконка "Есть сообщения мне"
		/// </summary>
		Me
	}
}