﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;
using System.Windows.Forms;

using Microsoft.Win32;

using Rsdn.Janus.Utils;
using Rsdn.SmartApp;

namespace Rsdn.Janus
{
	/// <summary>
	/// Вспомогательные функции для получения информации о среде выполения.
	/// </summary>
	public static class EnvironmentHelper
	{
		private const string _unknownOs = "Unknown Win32 compatible OS";
		private static readonly Assembly _janusAssembly = Type.GetType("Rsdn.Janus.Janus, Janus").Assembly;

		public static Assembly JanusAssembly
		{
			get { return _janusAssembly; }
		}

		public static string GetOSName(this OperatingSystem os)
		{
			switch (os.Platform)
			{
				case PlatformID.Win32Windows:
					if (os.Version.Major == 4)
						switch (os.Version.Minor)
						{
							case 0:
								return "Windows 95";
							case 10:
								return "Windows 98";
							case 90:
								return "Windows ME";
						}
					return _unknownOs;

				case PlatformID.WinCE:
					return "Windows CE";

				// TODO: Windows 2003 R2, Windows 2008, Windows 2008 R2 determination required
				case PlatformID.Win32NT:
					switch (os.Version.Major)
					{
						case 3:
							return "Windows NT 3.51";
						case 4:
							return "Windows NT 4.0";
						case 5:
							switch (os.Version.Minor)
							{
								case 0:
									return "Windows 2000";
								case 1:
									return "Windows XP";
								case 2:
									return "Windows 2003";
							}
							break;
						case 6:
							switch (os.Version.Minor)
							{
								case 0:
									return "Windows Vista";
								case 1:
									return "Windows 7";
								case 2:
									return "Windows 8";
							}
							break;
					}

					return _unknownOs;

				default:
					return _unknownOs;
			}
		}

		public static string GetOSNameWithVersion(this OperatingSystem os)
		{
			return GetOSName(os) + " " + os.Version;
		}

		public static bool IsModernOS(this OperatingSystem os)
		{
			return os.Platform == PlatformID.Win32NT && os.Version.Major > 4;
		}

		public static string GetAssemblyDir(this Assembly assembly)
		{
			if (assembly == null)
				throw new ArgumentNullException("assembly");
			var uri = new Uri(assembly.CodeBase);
			// Склеиваем все сегменты, кроме первого и последнего.
			return
				uri
					.Segments
					.Skip(1)
					.Take(uri.Segments.Length - 2)
					.Select(HttpUtility.UrlDecode)
					.JoinStrings();
		}

		public static string GetJanusRootDir()
		{
			return Assembly.GetEntryAssembly().GetAssemblyDir();
		}

		public static void SetSplashMessage(this IServiceProvider provider, string message)
		{
			var infSvc = provider.GetService<IBootTimeInformer>();
			if (infSvc != null)
				infSvc.SetText(message);
		}

		public static bool IsSplashAvailable(this IServiceProvider provider)
		{
			var infSvc = provider.GetService<IBootTimeInformer>();
			return infSvc != null;
		}

		public static bool IsComRegistered(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			var strClsId = "{" + Marshal.GenerateGuidForType(type).ToString().ToUpper(CultureInfo.InvariantCulture) + "}";

			using (var rootKey = Registry.ClassesRoot.OpenSubKey(@"CLSID", false))
			{
				if (rootKey == null)
					return false;

				using (var clsIdKey = rootKey.OpenSubKey(strClsId, false))
				{
					if (clsIdKey == null)
						return false;

					using (var inProcServerKey = clsIdKey.OpenSubKey(@"InprocServer32", false))
					{
						if (inProcServerKey == null)
							return false;

						var assembly = type.Assembly;

						if (!IsComRegisteredInNode(type, assembly, inProcServerKey))
							return false;

						var versionStr = assembly.GetName().Version.ToString(4);
						using (var versionSubKey = inProcServerKey.OpenSubKey(versionStr, false))
						{
							if (versionSubKey == null)
								return false;

							return IsComRegisteredInNode(type, assembly, versionSubKey);
						}
					}
				}
			}
		}

		private static bool IsComRegisteredInNode(Type type, Assembly assembly, RegistryKey inProcServerKey)
		{
			var regFullName = inProcServerKey.GetValue(@"Class") as string;
			if (regFullName != type.FullName)
				return false;

			var regAsmName = inProcServerKey.GetValue(@"Assembly") as string;
			if (regAsmName != assembly.FullName)
				return false;

			var regRuntimeVersion = inProcServerKey.GetValue(@"RuntimeVersion") as string;
			if (regRuntimeVersion != assembly.ImageRuntimeVersion)
				return false;

			var regCodeBase = inProcServerKey.GetValue(@"CodeBase") as string;
			if (regCodeBase != assembly.CodeBase)
				return false;

			return true;
		}

		public static void ComRegister(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			var rs = new RegistrationServices();
			rs.RegisterAssembly(type.Assembly, AssemblyRegistrationFlags.SetCodeBase);
		}

		public static ExitCode RunAsAdmin(string arguments)
		{
			if (arguments == null)
				throw new ArgumentNullException("arguments");

			var startInfo = new ProcessStartInfo(Application.ExecutablePath, arguments)
			                	{
			                		Verb = "runas"
			                	};

			try
			{
				using (var process = Process.Start(startInfo))
				{
					process.WaitForExit();
					var res = process.ExitCode;

					return Enum.IsDefined(typeof(ExitCode), res)
						? (ExitCode)res
						: ExitCode.Unknown;
				}
			}
			catch
			{
				return ExitCode.Error;
			}
		}
	}
}