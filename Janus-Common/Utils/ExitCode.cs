﻿namespace Rsdn.Janus.Utils
{
	/// <summary>
	/// Дос код завершения процесса
	/// </summary>
	public enum ExitCode
	{
		Unknown = -1024,
		Error = -1,
		Ok = 0,
		AnotherInstanceDetected,
		CheckEnvironmentFailed
	}
}