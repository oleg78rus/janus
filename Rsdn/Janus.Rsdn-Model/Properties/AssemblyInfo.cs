﻿using System.Reflection;
using System.Runtime.InteropServices;
using Rsdn.Janus;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Janus.Rsdn-Model")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(ApplicationInfo.Company)]
[assembly: AssemblyProduct(ApplicationInfo.ApplicationName)]
[assembly: AssemblyCopyright(ApplicationInfo.Copyright)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("02df2698-54bd-436c-93ee-dd26d69e0db8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(ApplicationInfo.VersionString)]
[assembly: AssemblyFileVersion(ApplicationInfo.VersionString)]