﻿using System.Reflection;
using System.Runtime.InteropServices;

using Rsdn.Janus;

[assembly: AssemblyTitle("Janus")]
[assembly: AssemblyDescription("Janus main application")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(ApplicationInfo.Company)]
[assembly: AssemblyProduct(ApplicationInfo.ApplicationName)]
[assembly: AssemblyCopyright(ApplicationInfo.Copyright)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion(ApplicationInfo.VersionString)]
[assembly: AssemblyFileVersion(ApplicationInfo.VersionString)]
