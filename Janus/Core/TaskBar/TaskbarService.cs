﻿using System;

using System.Collections.Generic;

using System.ComponentModel;

using System.Drawing;

using System.Linq;

using JetBrains.Annotations;

using Microsoft.WindowsAPICodePack.Taskbar;

using Rsdn.Janus.Core.NotifyIcon;

using Rsdn.SmartApp;

namespace Rsdn.Janus.TaskBar
{
	[Service(typeof(ITaskbarService))]
	public class TaskbarService : ITaskbarService, ITaskIndicatorProvider, ISyncProgressVisualizer, ISyncErrorInformer,
								  ITaskIndicator
	{
		#region Fields

		private static Icon _overlayMe;
		private static Icon _overlayNew;

		private readonly IServiceProvider _serviceManager;
		private readonly IForumsAggregatesService _forumsAggregatesService;
		private IDisposable _aggregatesChangedSubscription;
		private readonly AsyncOperation _uiAsyncOperation;
		private IntPtr _handle;
		private readonly List<SyncErrorInfo> _errors = new List<SyncErrorInfo>();
		private bool _errorMode;
		private const string _skippedTextSubst = "-//-";
		private readonly object _errorsLock = new object();
		private readonly IMenuService _menuService;
		private readonly IStyleImageManager _styleImageManager;
		private readonly ICommandHandlerService _commandHandlerService;

		#endregion

		#region Properties

		internal IntPtr ProgressHandle { private get; set; }

		#endregion

		#region Constructor

		public TaskbarService([NotNull] IServiceProvider serviceProvider)
		{
			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			_serviceManager = new ServiceManager(serviceProvider);

			var assembly = typeof(TaskbarService).Assembly;
			_overlayMe = assembly.LoadIcon(ApplicationInfo.ResourcesNamespace + "OverlayMe.ico");
			_overlayNew = assembly.LoadIcon(ApplicationInfo.ResourcesNamespace + "OverlayNew.ico");

			_uiAsyncOperation = _serviceManager
				.GetRequiredService<IUIShell>()
				.CreateUIAsyncOperation();

			_forumsAggregatesService = _serviceManager.GetRequiredService<IForumsAggregatesService>();
			_menuService = _serviceManager.GetRequiredService<IMenuService>();
			_styleImageManager = _serviceManager.GetRequiredService<IStyleImageManager>();
			_commandHandlerService = _serviceManager.GetRequiredService<ICommandHandlerService>();
		}

		#endregion

		#region Methods

		private void UpdateIcon()
		{
			var iconType = OverlayIconType.None;
			string text = null;

			var totalUnread = _forumsAggregatesService.UnreadMessagesCount;
			var meUnread = _forumsAggregatesService.UnreadRepliesToMeCount;

			if (totalUnread > 0)
			{
				if (meUnread > 0)
				{
					text = "{0} {1}".FormatStr(
						meUnread,
						meUnread.GetDeclension(
							NotifyIconResources.HintMeMessages1,
							NotifyIconResources.HintMeMessages2,
							NotifyIconResources.HintMeMessages5));
					iconType = OverlayIconType.Me;
				}
				else
				{
					text = "{0} {1}".FormatStr(
						totalUnread,
						totalUnread.GetDeclension(
							NotifyIconResources.HintUnreadMessages1,
							NotifyIconResources.HintUnreadMessages2,
							NotifyIconResources.HintUnreadMessages5));
					iconType = OverlayIconType.New;
				}
			}

			SetIcon(iconType, text);
		}

		private void SetIcon(OverlayIconType iconTypeType, string text)
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}

			Icon icon = null;
			switch (iconTypeType)
			{
				case OverlayIconType.New:
					icon = _overlayNew;
					break;

				case OverlayIconType.Me:
					icon = _overlayMe;
					break;
			}

			_uiAsyncOperation.Post(() => TaskbarManager.Instance.SetOverlayIcon(_handle, icon, text));
		}

		internal void StopProgress()
		{
			ProgressHandle = IntPtr.Zero;
			lock (_errorsLock)
			{
				_errorMode = false;
				_errors.Clear();
			}
			WindowsTaskbarSetProgressState(TaskbarProgressBarState.NoProgress);
		}

		private bool HasSameErrorText(SyncErrorInfo errorInfo)
		{
			var i = _errors.Count - 1;
			while (i >= 0)
			{
				var cur = _errors[i];
				if (cur.Text != _skippedTextSubst)
					return cur.Text == errorInfo.Text;
				i--;
			}
			return false;
		}

		private void WindowsTaskbarSetProgressState(int current, int max)
		{
			var state = max >= 0 ? TaskbarProgressBarState.Normal : TaskbarProgressBarState.Indeterminate;
			WindowsTaskbarSetProgressState(state, current, max > 0 ? max : (int?)null);
		}

		private void WindowsTaskbarSetProgressState(TaskbarProgressBarState state)
		{
			WindowsTaskbarSetProgressState(state, null, null);
		}

		private void WindowsTaskbarSetProgressState(TaskbarProgressBarState state, int? current, int? max)
		{
			lock (_errorsLock)
			{
				if (state == TaskbarProgressBarState.Normal && _errorMode)
				{
					state = TaskbarProgressBarState.Error;
				}
			}

			_uiAsyncOperation.Post(() =>
			{
				var taskbar = TaskbarManager.Instance;

				var handle = ProgressHandle == IntPtr.Zero ? _handle : ProgressHandle;

				taskbar.SetProgressState(state, handle);

				if (!current.HasValue || !max.HasValue)
				{
					return;
				}

				taskbar.SetProgressValue(current.Value, max.Value, handle);
			});
		}

		private void AddButtonsFromMenu(string menuName, string[] commandNames)
		{
			var menu = _menuService.GetMenu(menuName);

			foreach (
				var menuCommand in
					menu.Groups.SelectMany(
						menuGroup =>
						menuGroup.Items.OfType<IMenuCommand>().Where(
							menuCommand => Array.Exists(commandNames, n => menuCommand.CommandName == n))))
			{
				AddButton(menuCommand);
			}
		}

		private void AddButton(IMenuCommand menuCommand)
		{
			var text = menuCommand.Text;
			var image = _styleImageManager.TryGetImage(menuCommand.Image, StyleImageType.Large);

			var button = new ThumbnailToolBarButton(image.ToIcon(), text);
			button.Click += delegate
			{
				_commandHandlerService.ExecuteCommand(
					menuCommand.CommandName,
					new CommandContext(_serviceManager, menuCommand.Parameters));
			};
			_commandHandlerService.SubscribeCommandStatusChanged(_serviceManager,
																 delegate(ICommandHandlerService sender, string[] commandNames)
																 {
																	 var cmdName = menuCommand.CommandName;
																	 if (!Array.Exists(commandNames, n => cmdName == n))
																	 {
																		 return;
																	 }
																	 var commandStatus = _commandHandlerService.QueryStatus(
																		 cmdName,
																		 new CommandContext(_serviceManager, menuCommand.Parameters));
																	 button.Enabled = commandStatus == CommandStatus.Normal;
																 });

			TaskbarManager.Instance.ThumbnailToolBars.AddButtons(_handle, button);
		}

		#endregion

		#region Implementation of ITaskbarService

		public void Start(IntPtr handle)
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}

			_handle = handle;

			if (_aggregatesChangedSubscription == null)
			{
				_aggregatesChangedSubscription = _forumsAggregatesService.AggregatesChanged.Subscribe(args => UpdateIcon());
			}
			UpdateIcon();
		}

		public void Stop()
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}
			if (_aggregatesChangedSubscription != null)
			{
				_aggregatesChangedSubscription.Dispose();
				_aggregatesChangedSubscription = null;
			}

			SetIcon(OverlayIconType.None, null);
		}

		public void AddButtons(Dictionary<string, string[]> dictionary)
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}

			foreach (var key in dictionary.Keys)
			{
				AddButtonsFromMenu(key, dictionary[key]);
			}
		}

		#endregion

		#region Implementation of ISyncProgressVisualizer

		public void ReportProgress(int total, int current)
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}

			WindowsTaskbarSetProgressState(current, total);
		}

		public void SetProgressText(string text)
		{
		}

		public void SetCompressionSign(CompressionState state)
		{
		}

		#endregion

		#region Implementation of ISyncErrorInformer

		public void AddError(SyncErrorInfo errorInfo)
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}

			// Drop text, if previous error was the same
			lock (_errorsLock)
			{
				if (HasSameErrorText(errorInfo))
					errorInfo = new SyncErrorInfo(errorInfo.Type, errorInfo.TaskName, _skippedTextSubst);

				_errors.Add(errorInfo);
				if (!_errorMode)
				{
					_errorMode = true;
				}
			}
		}

		#endregion

		#region Implementation of ITaskIndicatorProvider

		public ITaskIndicator AppendTaskIndicator(string taskName)
		{
			lock (_errorsLock)
			{
				_errorMode = false;
				_errors.Clear();
			}
			return this;
		}

		#endregion

		#region Implementation of ITaskIndicator

		public void SetTaskState(SyncTaskState state)
		{
			if (!TaskbarManager.IsPlatformSupported)
			{
				return;
			}

			switch (state)
			{
				case SyncTaskState.WaitForSync:
				case SyncTaskState.Sync:
					WindowsTaskbarSetProgressState(TaskbarProgressBarState.Indeterminate);
					break;

				case SyncTaskState.Succeed:
				case SyncTaskState.Failed:
					WindowsTaskbarSetProgressState(TaskbarProgressBarState.NoProgress);
					break;
			}
		}

		public void SetStatusText(string text)
		{
		}

		#endregion
	}
}
